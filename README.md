Laravel gReCaptcha
------------------

Edit the config/app.php

    // Providers

    'providers' => [
        ...,
        Backtheweb\ReCaptcha\ReCaptchaServiceProvider::class
    ];

    //Alias

    'aliases' => [
        ...,
        'ReCaptcha' =>  Backtheweb\ReCaptcha\Facade::class
    ],


## Publish

    php artisan vendor:publish --provider="Backtheweb\ReCaptcha\ReCaptchaServiceProvider" --tag="config"


## How to use 

    $request->validate([
        'recaptcha'  => 'required|recaptcha',
    ]);
