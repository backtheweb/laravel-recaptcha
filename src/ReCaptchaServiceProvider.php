<?php

namespace Backtheweb\ReCaptcha;

use Illuminate\Support\ServiceProvider;

class ReCaptchaServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $app = $this->app;

        $this->publishes([__DIR__.'/../config/recaptcha.php' => config_path('recaptcha.php')], 'config');

        $app['validator']->extend('recaptcha', function ($attribute, $value) use ($app) {

            return $app['recaptcha']->verifyResponse($value, $app['request']->getClientIp());
        });
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->singleton('recaptcha', function ($app) {

            $config = $app['config']['recaptcha'];

            return new ReCaptcha($config['secret'], $config['key'], $config['attributes']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['recaptcha'];
    }

}
